FROM openjdk:8-jdk-alpine
VOLUME /tmp
VOLUME /logs
COPY  /target/spring-boot-ci-0.0.1-SNAPSHOT.jar app.jar
RUN chmod +x app.jar
ENV JAVA_OPTS="-Xmx512m -Xms128m"
ENV PROFILE=dev
ENV PORT 5000
EXPOSE $PORT
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Duser.timezone=Asia/Shanghai","${JAVA_OPTS}","${PROFILE}","-Dserver.port=${PORT}","-jar","/app.jar"]