package com.kancy.devops.springbootci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SpringBootCiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCiApplication.class, args);
    }

    @GetMapping
    public String index(){
        return String.format("Hello SpringBootCi : %s ", System.currentTimeMillis());
    }

}
